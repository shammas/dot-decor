/**
 * Created by psybo-03 on 12/12/17.
 */

var app = angular.module('myApp', ['ngRoute', 'ui.bootstrap', 'ngFileUpload', 'cp.ngConfirm', 'angular-loading-bar', 'datatables', 'datatables.buttons', 'datatables.colreorder', 'datatables.fixedheader']);
app.config(['$routeProvider', '$locationProvider', 'cfpLoadingBarProvider', function ($routeProvider, $locationProvider, cfpLoadingBarProvider) {
    cfpLoadingBarProvider.spinnerTemplate = '<div id="loading"></div>';
    cfpLoadingBarProvider.latencyThreshold = 500;

    $locationProvider.hashPrefix('');
    $routeProvider
        .when('/', {
            templateUrl: 'dashboard/index/dashboard'
        })
        .when('/dashboard', {
            templateUrl: 'dashboard/index/dashboard',
            controller: 'DashboardController'
        })
        .when('/projects', {
            templateUrl: 'dashboard/index/projects',
            controller: 'ProjectController'
        })
        .when('/testimonials', {
            templateUrl: 'dashboard/index/testimonial',
            controller: 'TestimonialController'
        })
        .otherwise({
            templateUrl: 'dashboard/index/dashboard'
        });

}]);

//Pagination filter
app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});


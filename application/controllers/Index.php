<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Index extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->library(['email', 'pagination']);
        $this->load->model('Project_model', 'project');
        $this->load->model('Project_file_model', 'project_file');
        $this->load->model('Testimonial_model', 'testimonial');
        $this->load->library(['form_validation']);
    }

    function index()
    {
        $data['testimonials'] = $this->testimonial->get_all();
        $this->load->view('index', $data);
    }

    function about()
    {
        $this->load->view('about');
    }

    function project()
    {
        $data['projects'] = $this->project->with_files()->get_all();
        $this->load->view('projects', $data);
    }

    public function projectSingle($id)
    {
        $data['next'] = $this->project->next($id);
        $data['previous'] = $this->project->previous($id);
        $data['project'] = $this->project->where('id', $id)->with_files()->with_category()->with_country()->get();

        $this->load->view('single-project', $data);
    }

    public function contact()
    {
        $this->load->view('contact');
    }

    public function testimonial()
    {
        $data['testimonials'] = $this->testimonial->get_all();
        $this->load->view('testimonials', $data);
    }

    public function sendContact()
    {
        $config = [
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.nuctaweb.com',
            'smtp_port' => 465,
            'smtp_user' => 'mail@nuctaweb.com',
            'smtp_pass' => 'mail@nucta',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        ];

        $this->email->initialize($config);

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');

        if ($this->form_validation->run() === FALSE ) {
            $error = $this->form_validation->get_errors();

            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_output(json_encode($error));

        } else {

            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $subject = $this->input->post('subject');
            $comment = $this->input->post('comment');

            $this->email->from('mail@nuctaweb.com', 'Dotdecor');
            $this->email->to('noushidpsybo@gmail.com');
            $this->email->subject('Contact Request From Web');


            $content = 'Name    :   ' . $name . PHP_EOL . PHP_EOL;
            $content .= 'Email    :   ' . $email . PHP_EOL . PHP_EOL;
            $content .= 'Subject   :   ' . $subject . PHP_EOL . PHP_EOL;
            $content .= 'Comment   :   ' . $comment . PHP_EOL . PHP_EOL;

            $content = str_replace("\n.", "\n.", $content);

            $this->email->message($content);

            if ($this->email->send()) {
                $this->output->set_output('success');
            } else {
                $this->output->set_output('error');
            }
        }
    }

}
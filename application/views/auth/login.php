<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Dot decor | Login</title>
    <link href="favicon.png" rel="shortcut icon" type="image/png">
    <link rel="stylesheet" href="assets/dashboard/css/vendor.css" />
    <link rel="stylesheet" href="assets/dashboard/css/app.css" />


</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <h3>Welcome to Dot decor</h3>
        <p>Perfectly designed and precisely prepared admin pages with extra new web app views.
        </p>
        <p>Login in. To see it in action.</p>
        <div id="infoMessage"><?php echo $message;?></div>

        <?php echo form_open(base_url('login'), ['class' => 'm-t', 'role' => 'form']);?>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Username" required="" name="identity" id="identity">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="Password" required="" name="password" id="password">
        </div>
        <button type="submit" class="btn btn-primary block full-width m-b" >Login</button>

        <a href="auth/forgot_password"><small>Forgot password?</small></a>
        </form>
    </div>
</div>
</body>

</html>

<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
    <meta property="og:title" content="Dot Decor | Customised Furnitures & Interior designers - Buy Custom Design Home Interiors & Furnitures" />
    <meta property="og:site_name" content="Dot Decor | Customised Furnitures & Interior designers" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="Dot Decor - We are a vertically integrated interior design and furniture company. We custom design home interiors, show you how your new home will look in 3D, manufacture the furniture at our factory and undertake project execution." />
    <meta property="og:type" content="website" />
    <title>Dot Decor - Customised Furnitures & Interior designers - Buy Custom Design Home Interiors & Furnitures</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="Dot Decor - We are a vertically integrated interior design and furniture company. We custom design home interiors, show you how your new home will look in 3D, manufacture the furniture at our factory and undertake project execution.">
    <meta name="keywords" content="full house furnishing, full home interiors, home interiors, custom design furniture, interior designer, interior designs, modular kitchen, furniture online, wardrobes online, furniture online india, bedroom furniture, online furniture, home furniture online, living room furniture, office furniture" />
    <link rel="shortcut icon" href="<?= base_url();?>assets/images/favicon.png">
    <!-- ============ Style Sheets ============ -->
    <link rel="stylesheet" href="<?= base_url();?>assets/css/animate.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/et-line-icons.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/swiper.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/justified-gallery.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/revolution/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/revolution/css/navigation.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootsnav.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/style.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/responsive.css" />
</head>
<body>
    <header>
        <nav class="navbar navbar-default bootsnav bg-transparent nav-top-scroll">
            <div class="container wow fadeIn nav-header-container height-100px xs-height-70px xs-padding-15px-lr">
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-8 text-left">
                        <a class="logo" href="<?= base_url();?>">
                            <img src="<?= base_url();?>assets/images/logo.png" data-at2x="<?= base_url();?>assets/images/logo.png" class="default" alt="Dot-Decor">
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 text-right">
                        <div class="hamburger-menu">
                            <div class="btn btn-hamburger border-none maine-btn">
                                <button class="navbar-toggle mobile-toggle" type="button" id="open-button" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>
                            <div class="hamburger-menu-wrepper xs-text-center">
                                <div class="hamburger-logo text-left">
                                    <a href="#" class="logo">
                                        <img src="<?= base_url();?>assets/images/logo.png" data-at2x="<?= base_url();?>assets/images/logo.png" alt="Dot-Decor"/>
                                    </a>
                                </div>
                                <div class="btn btn-hamburger border-none">
                                    <button class="close-menu close-button-menu" id="close-button"></button>
                                </div>
                                <div class="animation-box">
                                    <div class="menu-middle">
                                        <div class="menu-wrapper display-table-cell vertical-align-middle text-left">
                                            <div class="equalize no-margin">
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 display-table">
                                                    <div class="display-table-cell vertical-align-middle">
                                                        <ul class="hamburger-menu-links alt-font">
                                                            <li><a href="<?= base_url();?>">Home</a></li>
                                                            <li><a href="<?= base_url();?>about">About Us</a></li>
                                                            <li><a href="<?= base_url();?>projects">Projects</a></li>
                                                            <li><a href="<?= base_url();?>contact">Contact</a></li>
                                                            <li><a href="<?= base_url();?>testimonials">Testimonials</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 display-table">
                                                    <div class="display-table-cell vertical-align-middle social-style-3">
                                                        <span class="text-extra-large text-deep-pink alt-font display-block margin-15px-bottom font-weight-400">Indian based Customised furniture and Interior Planners</span>
                                                        <span class="text-medium alt-font display-block font-weight-300 margin-15px-bottom line-height-30 fw">
                                                            Paravathani Building<br>
                                                            Ooty Road, Manjeri- 676 121,<br/>
                                                            Malappuram, Kerala - IND.<br/>
                                                            Mobile: +91-9995-424-211<br/>
                                                            Land line: 0483-2766-431<br/>
                                                            Email - <a href="mailto:art@dotdecor.in" class="text-white" target="_blank">art@dotdecor.in </a>
                                                        </span>
                                                        <div class="separator-line-horrizontal-medium-light2 bg-deep-pink margin-25px-tb xs-margin-15px-tb display-inline-block"></div>
                                                        <div class="social-icon-style-9">
                                                            <ul class="small-icon">
                                                                <li><a class="margin-20px-right facebook" href="https://www.facebook.com/dotatmos" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                                <li><a class="margin-20px-right google" href="https://www.instagram.com/dot.decor/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                                                <li><a class="margin-20px-right google" href="https://www.youtube.com/channel/UCFWR8SphsHI9maNByQNJ8SA" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <section class="no-padding main-slider height-100 mobile-height wow fadeIn ">
        <div class="swiper-full-screen swiper-container height-100 width-100 black-move">
            <div class="swiper-wrapper">
                <div class="swiper-slide cover-background" style="background-image:url('<?= base_url();?>assets/images/slider-3-min.jpg');"></div>
                <div class="swiper-slide cover-background" style="background-image:url('<?= base_url();?>assets/images/slider-4-min.jpg');"></div>
                <!-- <div class="swiper-slide cover-background" style="background-image:url('<?= base_url();?>assets/images/slider-1-min.jpg');"></div> -->
                <div class="swiper-slide cover-background" style="background-image:url('<?= base_url();?>assets/images/slider-5-min.jpg');"></div>
                <!-- <div class="swiper-slide cover-background" style="background-image:url('<?= base_url();?>assets/images/slider-2-min.jpg');"></div> -->
            </div>
            <!-- start slider pagination -->
            <div class="swiper-pagination"></div>
            <div class="swiper-button-next swiper-button-black-highlight display-none"></div>
            <div class="swiper-button-prev swiper-button-black-highlight display-none"></div>
            <!-- end slider pagination -->
        </div>
    </section>
    <!-- <section class="wow fadeIn no-padding parallax xs-background-image-center" data-stellar-background-ratio="0.5" style="background-image:url('<?= base_url();?>assets/images/home-banner.jpg');">
        <div class="opacity-extra-medium bg-black"></div>
        <div class="container-fluid padding-thirteen-lr one-third-screen xs-padding-15px-lr">
            <div class="row height-100">
                <div class="position-relative height-100">
                    <div class="slider-typography">
                        <div class="slider-text-middle-main">
                            <div class="slider-text-bottom">
                                <div class="col-lg-12 text-center">
                                    <h4 class="text-white alt-font font-weight-300 width-60 center-col margin-ten-bottom md-margin-fifteen-bottom md-width-80 sm-margin-twenty-bottom xs-width-100 xs-margin-100px-bottom">We combine <strong>smart design</strong> with rich technology to <strong>craft innovative</strong> furnitures</h4>
                                </div>
                            </div>
                            <div class="down-section text-center">
                                <a href="#creative" class="inner-link"><i class="ti-arrow-down icon-medium text-deep-pink"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <section class="wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-11 col-xs-12 center-col text-center margin-three-bottom xs-margin-30px-bottom">
                    <div class="alt-font text-medium-gray margin-5px-bottom text-uppercase text-small">We love to Customise your Dreams</div>
                    <h6 class="font-weight-300 text-extra-dark-gray no-margin">
                        DOT. provides <strong class="font-weight-400">100% customised</strong> original designs with respect to customer <strong class="font-weight-400">taste and demand</strong>. Every piece of furniture designed and manufactured by us is <strong class="font-weight-400">unique in terms</strong> of its material, finish, size, colour and design.
                    </h6>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-6 col-xs-12 text-center feature-box-11 sm-margin-eight-bottom xs-margin-fifteen-bottom wow fadeInRight last-paragraph-no-margin">
                    <div class="display-inline-block padding-30px-all width-130px height-130px line-height-65 border-radius-100 bg-medium-gray text-center progress-line">
                        <img src="<?= base_url();?>assets/images/image-icon6.png" alt="">
                    </div>
                    <div class="alt-font margin-30px-top margin-5px-bottom text-capitalize font-weight-500">talk to our executives and get estimate</div>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12 text-center feature-box-11 sm-margin-eight-bottom xs-margin-fifteen-bottom wow fadeInRight last-paragraph-no-margin" data-wow-delay="0.4s">
                    <div class="display-inline-block padding-30px-all width-130px height-130px line-height-65 border-radius-100 bg-medium-gray text-center progress-line">
                        <img src="<?= base_url();?>assets/images/image-icon7.png" alt="">
                    </div>
                    <div class="alt-font margin-30px-top margin-5px-bottom text-capitalize font-weight-500">detailed drawing and customer approval</div>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12 text-center feature-box-11 xs-margin-fifteen-bottom wow fadeInRight last-paragraph-no-margin" data-wow-delay="0.8s">
                    <div class="display-inline-block padding-30px-all width-130px height-130px line-height-65 border-radius-100 bg-medium-gray text-center progress-line">
                        <img src="<?= base_url();?>assets/images/image-icon8.png" alt="">
                    </div>
                    <div class="alt-font margin-30px-top margin-5px-bottom text-capitalize font-weight-500">production at own factories</div>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12 text-center feature-box-11 sm-margin-eight-bottom xs-margin-fifteen-bottom wow fadeInRight last-paragraph-no-margin" data-wow-delay="1.2s">
                    <div class="display-inline-block padding-30px-all width-130px height-130px line-height-65 border-radius-100 bg-medium-gray text-center progress-line">
                        <img src="<?= base_url();?>assets/images/image-icon9.png" alt="">
                    </div>
                    <div class="alt-font margin-30px-top margin-5px-bottom text-capitalize font-weight-500">material delivery within 45-55 days</div>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12 text-center feature-box-11 xs-margin-fifteen-bottom wow fadeInRight last-paragraph-no-margin" data-wow-delay="1.6s">
                    <div class="display-inline-block padding-30px-all width-130px height-130px line-height-65 border-radius-100 bg-medium-gray text-center progress-line">
                        <img src="<?= base_url();?>assets/images/image-icon10.png" alt="">
                    </div>
                    <div class="alt-font margin-30px-top margin-5px-bottom text-capitalize font-weight-500">installation at site & on time hand over</div>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12 text-center feature-box-11 wow fadeInRight last-paragraph-no-margin" data-wow-delay="2.0s">
                    <div class="display-inline-block padding-30px-all width-130px height-130px line-height-65 border-radius-100 bg-medium-gray text-center">
                        <img src="<?= base_url();?>assets/images/image-icon11.png" alt="">
                    </div>
                    <div class="alt-font margin-30px-top margin-5px-bottom text-capitalize font-weight-500">lifetime customer service and support</div>
                </div>
            </div>
        </div>
    </section>
    <section id="creative" class="wow fadeIn">
        <div class="container">
            <div class="row equalize sm-equalize-auto">
                <div class="col-md-4 col-sm-8 col-xs-12 sm-center-col sm-text-center sm-margin-40px-bottom xs-margin-30px-bottom wow fadeIn last-paragraph-no-margin">
                    <p class="alt-font font-weight-500 text-small text-uppercase text-deep-pink margin-10px-bottom"><strong>Easy way to build customised furnitures</strong></p>
                    <h5 class="alt-font font-weight-500 text-extra-dark-gray width-95 sm-width-100 xs-margin-15px-bottom">Perfect powerful team for creative architectures.</h5>
                    <p class="width-85 sm-width-100">
                        In 1989, at an era when furniture shops did not sprout up in every corner of the street, Paravathani Furniture began its journey in Majeri, Malappuram by providing a large customer base with custom-made furniture fit to the personality of their homes. Being pioneers in designing and manufacturing furniture which was unique and ahead of the times, it is only fit that Paravathani move into the next phase of furniture innovation with our new brand, DOT.
                    </p>
                    <a class="btn btn-dark-gray btn-small margin-30px-top sm-margin-20px-top xs-margin-15px-top" href="<?= base_url();?>about">About Company</a>
                </div>
                <div class="col-md-8 col-xs-12">
                    <div class="row blog-post">
                        <div class="col-md-6 col-sm-6 col-xs-12 wow fadeIn xs-margin-30px-bottom last-paragraph-no-margin" data-wow-delay="0.2s">
                            <div class="blog-post-images overflow-hidden margin-25px-bottom xs-margin-15px-bottom">
                                <img src="<?= base_url();?>assets/images/custom-furniture.jpg" alt="">
                            </div>
                            <div class="separator-line-verticle-small-thick bg-deep-pink display-inline-block vertical-align-top margin-two-half-top margin-four-right xs-display-none"></div>
                            <div class="post-details width-90 display-inline-block xs-width-100 xs-text-center">
                                <span class="alt-font margin-5px-bottom display-block text-extra-dark-gray font-weight-500">Customised Furnitures designer</span>
                                <p>
                                    With the best quality assured, we provide furniture fit for all budgets without any compromise on its quality. With a mission to make every home unique with originally crafted furniture, we provide after sales services including maintenance.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 wow fadeIn last-paragraph-no-margin" data-wow-delay="0.4s">
                            <div class="blog-post-images overflow-hidden margin-25px-bottom xs-margin-15px-bottom">
                                <img src="<?= base_url();?>assets/images/interior-design.jpg" alt="">
                            </div>
                            <div class="separator-line-verticle-small-thick bg-deep-pink display-inline-block vertical-align-top margin-two-half-top margin-four-right xs-display-none"></div>
                            <div class="post-details width-90 display-inline-block xs-width-100 xs-text-center">
                                <span class="alt-font margin-5px-bottom display-block text-extra-dark-gray font-weight-500">Creative Architecture and Designers</span>
                                <p>
                                    A team with over a 3-decade long experience, DOT provides 100% customised original designs with respect to customer taste and demand. Every piece of furniture designed and manufactured by unique in terms of its material, finish, size, colour and design.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-50px-top">
                <div class="col-md-3 col-sm-6 feature-box-12 sm-padding-35px-tb sm-center-col wow fadeInRight">
                    <div class="feature-icon-box icon-medium">
                        <div class="width-85px height-85px line-height-65 text-center">
                            <img src="<?= base_url();?>assets/images/image-icon1.png" alt="">
                        </div>
                    </div>
                    <div class="feature-content-box padding-30px-left">
                        <div class="alt-font text-extra-dark-gray text-medium">We Assure Material Quality</div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 feature-box-12 sm-padding-35px-tb sm-center-col wow fadeInRight" data-wow-delay="0.2s">
                    <div class="feature-icon-box icon-medium">
                        <div class="width-85px height-85px line-height-65 text-center">
                            <img src="<?= base_url();?>assets/images/image-icon2.png" alt="">
                        </div>
                    </div>
                    <div class="feature-content-box padding-30px-left">
                        <div class="alt-font text-extra-dark-gray text-medium">Work Done in<br/>45-55 days</div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 feature-box-12 sm-padding-35px-tb sm-center-col wow fadeInRight" data-wow-delay="0.4s">
                    <div class="feature-icon-box icon-medium">
                        <div class="width-85px height-85px line-height-65 text-center">
                            <img src="<?= base_url();?>assets/images/image-icon3.png" alt="">
                        </div>
                    </div>
                    <div class="feature-content-box padding-30px-left">
                        <div class="no-margin alt-font text-extra-dark-gray text-medium">Experienced Work Force</div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 feature-box-12 sm-padding-35px-tb sm-center-col wow fadeInRight" data-wow-delay="0.4s">
                    <div class="feature-icon-box icon-medium">
                        <div class="width-85px height-85px line-height-65 text-center">
                            <img src="<?= base_url();?>assets/images/image-icon4.png" alt="">
                        </div>
                    </div>
                    <div class="feature-content-box padding-20px-left">
                        <div class="no-margin alt-font text-extra-dark-gray text-medium">Life Time Customer Service</div>
                    </div>
                </div>
            </div> 
        </div>
    </section>
    <section class="wow fadeIn">
        <div class="container"> 
            <div class="row position-relative">
                <div class="swiper-slider-second swiper-pagination-bottom swiper-container black-move testimonial-style2 padding-35px-bottom sm-padding-50px-bottom xs-sm-padding-30px-bottom wow fadeIn">
                    <div class="swiper-wrapper">
                        <?php
                        if (isset($testimonials) and $testimonials != false) {
                            foreach ($testimonials as $testimonial) {
                                ?>
                                <!-- start testimonial slider item -->
                                <div class="swiper-slide">
                                    <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 center-col">
                                        <div class="testimonia-block width-90 xs-width-100 center-col">
                                            <div
                                                class="text-center padding-60px-all border-radius-6 xs-padding-20px-all">
                                                <p><?= $testimonial->description;?>.</p>
                                            </div>
                                            <div class="profile-box">
                                                <img src="<?= base_url() . $testimonial->url; ?>" alt=""
                                                     class="width-20 xs-center-col border-radius-100 border-color-white border-width-4 border-solid margin-15px-bottom">

                                                <div class="width-100 last-paragraph-no-margin">
                                                    <p class="alt-font text-small font-weight-500 text-black text-uppercase margin-5px-bottom">
                                                        <?= $testimonial->name;?></p>

                                                    <p class="text-uppercase text-extra-small text-medium-gray"><?= $testimonial->designation;?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- start testimonial slider item -->
                            <?php
                            }
                        }
                        ?>

                    </div>
                    <!-- start slider pagination -->                   
                    <div class="swiper-pagination swiper-pagination-second"></div>
<!--                    <div class="swiper-button-next slider-long-arrow-white display-none"></div>-->
<!--                    <div class="swiper-button-prev slider-long-arrow-white display-none"></div>-->
                    <!-- end slider pagination -->
                </div>
            </div>
        </div> 
    </section>
    <section class="wow fadeIn">
        <div class="container position-relative">
            <div class="row equalize sm-equalize-auto">
                <!-- start feature box item -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 display-table md-margin-15px-top sm-no-margin-top sm-margin-ten-bottom xs-padding-five-lr xs-margin-ten-bottom wow fadeIn last-paragraph-no-margin">
                    <div class="display-table-cell vertical-align-middle padding-fourteen-right sm-no-padding-right sm-text-center">
                        <h5 class="alt-font text-extra-dark-gray font-weight-500">Everything Begins With A DOT, Doting On Originality</h5>
                        <p class="width-85 md-width-100 xs-width-100 sm-margin-lr-auto text-medium-gray">
                            A team with over a 3-decade long experience, DOT. provides 100% customised original designs with respect to customer taste and demand. Every piece of furniture designed and manufactured by us is unique in terms of its material, finish, size, colour and design. We never replicate a piece of furniture twice, ensuring that your homes are made completely yours with furniture that no one else has or will have. We bring novelty to each furniture designed, ensuring that we keep up with the times and even stay one step ahead of the times. 
                        </p>
                        <a href="<?= base_url();?>projects" class="margin-35px-top btn btn-small btn-dark-gray sm-margin-30px-top">view more works<i class="ti-arrow-right"></i></a>
                    </div>
                </div>
                <!-- start feature box item -->
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center display-table xs-margin-ten-bottom wow fadeIn" data-wow-delay="0.2s">
                    <div class="display-table-cell vertical-align-middle">
                        <a class="popup-youtube" href="https://www.youtube.com/watch?v=nrJtHemSPW4">
                            <img src="<?= base_url();?>assets/images/home-project-1.jpg" alt="" class="width-100">
                            <div class="icon-play">
                                <div class="absolute-middle-center width-80">
                                    <img src="<?= base_url();?>assets/images/icon-play.png" alt="">
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- start feature box item -->
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center display-table xs-margin-ten-bottom wow fadeIn" data-wow-delay="0.2s">
                    <div class="display-table-cell vertical-align-middle">
                        <a class="popup-youtube" href="https://www.youtube.com/watch?v=nrJtHemSPW4">
                            <img src="<?= base_url();?>assets/images/home-project-2.jpg" alt="" class="width-100">
                            <div class="icon-play">
                                <div class="absolute-middle-center width-80">
                                    <img src="<?= base_url();?>assets/images/icon-play.png" alt="">
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn padding-80px-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 center-col text-center">
                    <h6 class="alt-font text-extra-dark-gray text-uppercase font-weight-500 width-80 center-col margin-35px-bottom md-width-100 wow fadeInUp">We would love to hear from you .</h6>
                    <a href="<?= base_url();?>contact" class="btn btn-medium btn-rounded btn-deep-pink wow fadeInUp" data-wow-delay="0.2s">Let’s Meet</a>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer-modern padding-five-tb xs-padding-30px-tb">
        <div class="footer-widget-area padding-40px-bottom xs-padding-30px-bottom">
            <div class="container">
                <div class="row equalize xs-equalize-auto">
                    <div class="col-md-4 col-sm-12 col-xs-12 xs-text-center sm-margin-three-bottom xs-margin-20px-bottom display-table">
                        <div class="display-table-cell vertical-align-middle">
                            <h6 class="text-dark-gray width-70 md-width-100 no-margin-bottom">Indian based Customised furniture and Interior Planners</h6>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 xs-text-center xs-margin-20px-bottom display-table">
                        <div class="display-table-cell vertical-align-middle">
                            <span class="display-block">Paravathani Building<br>Ooty Road, Manjeri- 676 121,<br>Malappuram, Kerala - IND.</span>
                            <a href="mailto:art@dotdecor.in" title="art@dotdecor.in">art@dotdecor.in</a>   |   +91-9995-42-42-11
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 social-style-2 xs-text-center display-table">
                        <div class="display-table-cell vertical-align-middle">
                            <div class="social-icon-style-8">
                                <ul class="text-extra-small margin-20px-top xs-no-margin-bottom text-uppercase no-padding no-margin-bottom list-style-none">
                                    <li class="display-inline-block margin-10px-right">
                                        <a href="#"><img class="footer-logo" src="<?= base_url();?>assets/images/footer-logo.png" alt="Dot Decor"></a>
                                    </li>
                                    <!-- <li class="display-inline-block margin-10px-right">
                                        <a href="#" target="_blank"><img class="footer-logo2" src="<?= base_url();?>assets/images/paravathani.png" alt="Dot Decor"></a>
                                    </li> -->
                                </ul>
                            </div>
                            <div class="social-icon-style-8">
                                <ul class="text-extra-small margin-20px-top xs-no-margin-bottom text-uppercase no-padding no-margin-bottom list-style-none">
                                    <li class="display-inline-block margin-10px-right"><a href="https://www.facebook.com/dotatmos" target="_blank" title="Facebook">Facebook</a></li>
                                    <li class="display-inline-block margin-10px-right"><a href="https://www.instagram.com/dot.decor/" target="_blank" title="Instagram">Instagram</a></li>
                                    <li class="display-inline-block margin-10px-right"><a href="https://www.youtube.com/channel/UCFWR8SphsHI9maNByQNJ8SA" target="_blank" title="Twitter">YouTube</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="footer-bottom border-color-extra-light-gray border-top padding-40px-top xs-padding-30px-top">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 text-left text-small xs-text-center">Dot Decor - Customised Furnitures &copy; 2019</div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-right text-small xs-text-center">Proudly Designed by <a href="http://cloudbery.com/" target="_blank" title="Cloudbery Solutions"><img src="<?= base_url();?>assets/images/cloudbery.png" alt="Cloudbery Solutions"></a></div>
                </div>
            </div>
        </div>
    </footer>
    <p class="since">Since : 1989</p>
    <a class="scroll-top-arrow" href="javascript:void(0);"><i class="ti-arrow-up"></i></a>
    <!-- ======= CUSTOMER CONTACT FORM ======= -->
    <div class="buy-theme alt-font">
        <a href="#customer-contact" class="popup-with-form"><i class="fa fa-envelope"></i><span>Get an Estimate</span></a>
    </div>
    <form id="customer-contact" action="javascript:void(0)" method="post" class="white-popup-block mfp-hide col-md-5 no-padding center-col">
        <div class="padding-five-all bg-white border-radius-6 md-padding-seven-all">
            <div class="text-extra-dark-gray alt-font text-large font-weight-500 margin-30px-bottom">Get an Estimate</div> 
            <div>
                <div id="success-contact-form" class="no-margin-lr"></div>
                <input type="text" name="name" id="name" placeholder="Name*" class="input-bg">
                <input type="number" name="number" id="number" placeholder="Contact Number*" class="input-bg">
                <input type="number" name="abroad" id="abroad" placeholder="Contact Number (In Abroad)" class="input-bg">
                <input type="email" name="email" id="email" placeholder="E-mail" class="input-bg">
                <input type="text" name="location" id="location" placeholder="Project location*" class="input-bg">
                <textarea name="comment" id="comment" placeholder="Your Message" class="input-bg"></textarea>
                <button id="contact-us-button" type="submit" class="btn btn-small border-radius-4 btn-black">send message</button>
            </div>
        </div>
    </form>
    <!-- ======= DESIGNER CONTACT FORM ======= -->
    <div class="all-demo alt-font">
        <a href="#designer-contact" class="popup-with-form"><i class="fa fa-handshake-o"></i><span>Architects & Designers Enquiry</span></a>
    </div>
    <form id="designer-contact" action="javascript:void(0)" method="post" class="white-popup-block mfp-hide col-md-5 no-padding center-col">
        <div class="padding-five-all bg-white border-radius-6 md-padding-seven-all">
            <div class="text-extra-dark-gray alt-font text-large font-weight-500 margin-30px-bottom">Let’s Associate...</div> 
            <div>
                <div id="success-contact-form" class="no-margin-lr"></div>
                <input type="text" name="name" id="name" placeholder="Name*" class="input-bg">
                <input type="number" name="number" id="number" placeholder="Contact Number*" class="input-bg">
                <input type="text" name="firm" id="firm" placeholder="Your Company Name" class="input-bg">
                <input type="text" name="place" id="place" placeholder="Your Place" class="input-bg">
                <input type="date" name="date" id="date" placeholder="Choose your Meeting Date*" onClick="$(this).removeClass('placeholderclass')" class="input-bg dateclass placeholderclass">
                <textarea name="comment" id="comment" placeholder="Your Message" class="input-bg"></textarea>
                <button id="contact-us-button" type="submit" class="btn btn-small border-radius-4 btn-black">send message</button>
            </div>
        </div>
    </form>

    <!-- javascript libraries -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/modernizr.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/skrollr.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/smooth-scroll.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.appear.js"></script>
    <!-- menu navigation -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/bootsnav.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.nav.js"></script>
    <!-- animation -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/wow.min.js"></script>
    <!-- page scroll -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/page-scroll.js"></script>
    <!-- swiper carousel -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/swiper.min.js"></script>
    <!-- parallax -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.stellar.js"></script>
    <!-- magnific popup -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.magnific-popup.min.js"></script>
    <!-- portfolio with shorting tab -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/isotope.pkgd.min.js"></script>
    <!-- images loaded -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/imagesloaded.pkgd.min.js"></script>
    <!-- pull menu -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/classie.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/hamburger-menu.js"></script>
    <!-- counter  -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/counter.js"></script>
    <!-- fit video  -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.fitvids.js"></script>
    <!-- equalize -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/equalize.min.js"></script>
    <!-- justified gallery  -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/justified-gallery.min.js"></script>
    <!-- retina -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/retina.min.js"></script>
    <!-- revolution -->
    <script type="text/javascript" src="<?= base_url();?>assets/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/main.js"></script>
</body>
</html>




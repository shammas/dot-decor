<div class="wrapper wrapper-content animated fadeInRight" ng-if="user.menu[0].name=='dashboard'">
    <div class="row m-t-lg">
        <div class="col-lg-9">
            <div class="col-lg-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        Notification
                    </div>
                    <div class="panel-body">
                        <ul class="list-group clear-list m-t">
                            <li class="list-group-item fist-item">
                                <span class="label label-danger">{{status.totalPending}}</span> <span ><a href="#/applications">Pending Applications</a></span>
                            </li>
                            <li class="list-group-item">
                                <span class="label label-danger">{{countexamcenter}}</span> <a href="#/examcentre">Total Exam Centers</a>
                            </li>
                            <li class="list-group-item">
                                <span class="label label-danger">{{countemployee}}</span>  <a href="#/employee">Total Employee</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                    Hall Ticket Download Status
                    </div>
                    <div class="panel-body">
                        <ul class="list-group clear-list m-t">
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    <strong>{{downloadstatus.online}}</strong>
                                </span>
                                <span class="label label-primary">&nbsp;</span> Online
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">
                                    <strong>{{downloadstatus.app}}</strong>
                                </span>
                                <span class="label label-primary">&nbsp;</span> Application
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">
                                    <strong>{{downloadstatus.online+downloadstatus.app}}</strong>
                                </span>
                                <span class="label label-primary">&nbsp;</span> Total
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

<!--            <div class="col-lg-4">-->
<!--                <div class="panel panel-success">-->
<!--                    <div class="panel-heading">-->
<!--                        Application Status-->
<!--                    </div>-->
<!--                    <div class="panel-body">-->
<!--                        <div class="table">-->
<!--                            <table class="table table-responsive table-striped table-hover table-bordered">-->
<!--                                <tbody>-->
<!--                                <tr class="success">-->
<!--                                    <th></th><th>All</th><th>Approved</th>-->
<!--                                </tr>-->
<!--                                <tr class="warning">-->
<!--                                    <td>Online</td>-->
<!--                                    <td style="text-align: center">{{status.onlineTotal}}</td>-->
<!--                                    <td style="text-align: center">{{status.onlineAccept}}</td>-->
<!--                                </tr>-->
<!--                                <tr class="success">-->
<!--                                    <td>App</td>-->
<!--                                    <td style="text-align: center">{{status.appTotal}}</td>-->
<!--                                    <td style="text-align: center">{{status.appAccept}}</td>-->
<!--                                </tr>-->
<!--                                <tr class="danger" style="text-align: center">-->
<!--                                    <td>Total</td><td>{{status.onlineTotal+status.appTotal}}</td><td>{{status.onlineAccept+status.appAccept+status.offlineAccept}}</td>-->
<!--                                </tr>-->
<!---->
<!--                                </tbody>-->
<!--                            </table>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->


            <div class="col-lg-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        Admission Status
                    </div>
                    <div class="panel-body">
                        <div class="table">
                            <table class="table table-responsive table-striped table-hover table-bordered">
                                <tbody>
                                <tr class="warning" ng-show="latestCount.students.length > 0">
                                    <td style="text-align: center"><strong>Date</strong></td>
                                    <td style="text-align: center">Total</td>
                                </tr>
                                <tr class="warning" ng-repeat="stu in latestCount.students">
                                    <td style="text-align: center">{{stu.created_date | date:'dd-MM-yyyy'}}</td>
                                    <td style="text-align: center">{{stu.total}}</td>
                                </tr>
                                <tr class="warning">
                                    <td style="text-align: center"><strong>Total Application</strong></td>
                                    <td style="text-align: center">{{latestCount.total}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="panel panel-success">
                <div class="panel-heading">
                    School/Student Status
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <select name="" id="" class="selector form-control"  ng-change="loadSchoolStatus(school)" ng-model="school" >
                                <option value="all" selected>All</option>
                                <option value="{{school.id}}" ng-repeat="school in schools">{{school.name}}</option>
                            </select>
                        </li>
                        <li class="list-group-item" >
                            <div class="table">
                                <table class="table table-responsive table-striped table-hover">
                                    <tbody>
                                    <tr class="success">
                                        <td style="font-weight: bold">Total No. of Students</td>
                                        <td>-</td>
                                        <td style="font-weight: bold">{{schoolstatus.total}}</td>
                                    </tr>
                                    <tr class="warning">
                                        <td>Boys</td>
                                        <td>-</td>
                                        <td>{{schoolstatus.male}}</td>
                                    </tr>
                                    <tr class="success">
                                        <td>Girls</td>
                                        <td>-</td>
                                        <td>{{schoolstatus.female}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        Exam Centre/Students Status
                    </div>
                    <div class="panel-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <select name="" id="" class="selector form-control"  ng-change="loadexamcenterdtls(examcenter)" ng-model="examcenter" >
                                    <option value="all" selected>All</option>
                                    <option value="{{examcenter.id}}" ng-repeat="examcenter in examcenters">{{examcenter.name}}</option>
                                </select>
                            </li>
                            <li class="list-group-item" >
                                <div class="table">
                                    <table class="table table-responsive table-striped table-hover">
                                        <tbody>
                                        <tr class="success">
                                            <td style="font-weight: bold">Total No. of Students</td>
                                            <td>-</td>
                                            <td style="font-weight: bold">{{countstudents}}</td>
                                        </tr>
                                        <tr class="warning">
                                            <td>Boys</td>
                                            <td>-</td>
                                            <td>{{studentStatusCount.totalBoys}}</td>
                                        </tr>
                                        <tr class="success">
                                            <td>Girls</td>
                                            <td>-</td>
                                            <td>{{studentStatusCount.totalGirls}}</td>
                                        </tr>
                                        <tr><th></th></tr>
                                        <tr class="warning">
                                            <td>CBSE</td>
                                            <td>-</td>
                                            <td>{{studentStatusCount.totalCbse}}</td>
                                        </tr>
                                        <tr class="success">
                                            <td>State</td>
                                            <td>-</td>
                                            <td>{{studentStatusCount.totalState}}</td>
                                        </tr>
                                        <tr class="warning">
                                            <td ng-hide="countstudents==0" style="text-align: center;" colspan="3">{{rollstart}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </li>
                            <li class="list-group-item" >
                                <div class="table">
                                    <table class="table table-responsive table-striped table-hover">
                                        <tbody>
                                        <tr class="success">
                                            <td colspan="3" style="font-weight: bold">Solution Downloaded List</td>
                                        </tr>
                                        <tr class="warning">
                                            <td>Online</td>
                                            <td>-</td>
                                            <td>{{answerkeysStatus.online}}</td>
                                        </tr>
                                        <tr class="success">
                                            <td>App</td>
                                            <td>-</td>
                                            <td>{{answerkeysStatus.app}}</td>
                                        </tr>
                                        <tr class="warning">
                                            <td>Total</td>
                                            <td>-</td>
                                            <td class="text-danger font-bold">{{answerkeysStatus.app+answerkeysStatus.online}}</td>
                                            <!-- <td style="text-align: center;" colspan="3">{{answerkeysStatus.app+answerkeysStatus.online}}</td> -->
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="panel panel-success">
                <div class="panel-heading">
                    Application Entering Summery
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="input-group">
                                    <input type="text" class="form-control" uib-datepicker-popup is-open="fromDatePicker" close-text="Close" ng-model="fromDate" ng-click="fromDatePicker=true" />
                                      <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="fromDatePicker=true"><i class="glyphicon glyphicon-calendar"></i></button>
                                      </span>
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <p class="input-group">
                                    <input type="text" class="form-control" uib-datepicker-popup is-open="toDatePicker" close-text="Close" ng-model="toDate" ng-click="toDatePicker=true" />
                                      <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="toDatePicker=true"><i class="glyphicon glyphicon-calendar"></i></button>
                                      </span>
                                </p>
                                <button ng-click="getApplicationEnterStatus(fromDate,toDate);" type="button" class="btn btn-sm btn-primary"> Go!</button>
                            </div>

<!--                            <div class="col-sm-1">-->
<!--                                <button ng-click="getApplicationEnterStatus(fromDate,toDate);" type="button" class="btn btn-sm btn-primary"> Go!</button>-->
<!--                            </div>-->
                        </div>

                        <div class="table-responsive" ng-show="enterstatus.length > 0">
                            <table class="table table-striped table-bordered ">
                                <thead>
                                <th>User</th>
                                <th>Count</th>
                                </thead>
                                <tbody>
                                <tr ng-repeat="status in enterstatus">
                                    <td>{{status.name}}</td>
                                    <td>{{status.count}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Exam Centre Statistics<small class="m-l-sm">All Exam Centre Statics </small></h5>
                </div>
                <div class="ibox-content icons-box">
                    <div class="table-responsive" ng-show="centerstatitcs.length > 0">
                        <table class="table table-striped table-bordered" datatable="ng" dt-options="dtOptions">
                            <thead>
                            <th>#</th>
                            <th>Exam Centre</th>
                            <th>Total Form</th>
                            <th>Roll no Gen. pending</th>
                            <th>Hall Ticket Downloaded</th>
                            </thead>
                            <tbody>
                            <tr ng-repeat="centerstat in centerstatitcs">
                                <td>{{$index+1}}</td>
                                <td>{{centerstat.name}}</td>
                                <td>{{centerstat.student_count}}</td>
                                <td>{{centerstat.student_roll_pending_count}}</td>
                                <td>{{centerstat.student_hall_download_count}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight" ng-if="user.menu[0].name != 'dashboard'">
    <div class="row">
        No Permission to View this Page.
    </div>
</div>

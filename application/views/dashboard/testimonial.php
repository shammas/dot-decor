<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row" ng-show="showform">
        <div class="" ng-class="{'col-lg-12' : !files, 'col-lg-9' : files}">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add testimonial</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" ng-submit="addTestimonial()">
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Name</label>
                            <div class="col-lg-4"  ng-class="{'has-error' : validationError.name}">
                                <input type="text" placeholder="Name" class="form-control" ng-model="newtestimonial.name">
                            </div>

                            <label class="col-lg-2 control-label">Designation</label>
                            <div class="col-lg-4"  ng-class="{'has-error' : validationError.testimonial}">
                                <input type="text" placeholder="Designation" class="form-control" ng-model="newtestimonial.designation">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Description</label>
                            <div class="col-lg-8"  ng-class="{'has-error' : validationError.about}">
                                <textarea class="form-control" ng-model="newtestimonial.description"></textarea>
                            </div>

                        </div>

                        <div class="form-group" ng-class="{'has-error' : validationError.file}">
                            <label for="" class="control-label col-lg-2">Photo</label>
                            <div class="col-md-8">
                                <button ngf-select="uploadFiles($files, $invalidFiles)"
                                        accept="image/*"
                                        ngf-max-height="5000"
                                        ngf-max-size="5MB"
                                        ngf-multiple="true" type="button"
                                        class="upload-drop-zone btn-default"
                                        ngf-drop="uploadFiles($files)"
                                        ngf-drag-over-class="'drop'"
                                        ngf-multiple="false"
                                        ngf-pattern="'image/*'"
                                        ng-class="{'upload-drop-zone-error' : validationError.file}">
                                    <div class="dz-default dz-message">
                                        <span><strong>Drop files here or click to upload. </strong></span>
                                    </div>
                                </button>
                                <div class="help-block" ng-show="validationError.file">Please select image!</div>
                                <span class="alert alert-danger" ng-show="fileValidation.status == true">{{fileValidation.msg}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-primary" type="submit" ng-bind="(curtestimonial == false ? 'Add' : 'Update')">Add</button>
                                <button class="btn btn-danger" type="button" ng-click="hideForm()">Cancel</button>
                            </div>
                        </div>
                    </form>

                    <div class="col-lg-5">
                        <div class="lightBoxMoment" ng-show="curtestimonial">
                            <div class="col-md-2">
                                <a class="example-image-link" href="{{curtestimonial.url}}" data-lightbox="item-list-{{curtestimonial.name}}">
                                    <img src="{{curtestimonial.url}}" width="50px">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3" ng-show="files">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Upload status</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <h5>{{files.name}}</h5>
                        <div class="lightBoxGallery">
                            <a class="example-image-link" href="{{files.$ngfBlobUrl}}" data-lightbox="example-1" data-title="">
                                <img ngf-src="files.$ngfBlobUrl" alt=""  style="width: 25px; max-height: 25px" id="myImg"/>
                            </a>
                        </div>
                        <div class="progress">
                            <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="35" role="progressbar"
                                 class="progress-bar progress-bar-success"
                                 style="width:{{files.progress}}%" ng-show="uploadstatus != 1">
                                <span>{{files.progress}}% Complete</span>
                            </div>
                        </div>
                    </div>
                    <p class="text-danger" ng-repeat="f in errFiles">{{files.name}} {{files.$error}} {{files.$errorParam}}.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Show All testimonials</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-responsive" datatable="ng" dt-options="dtOptions">
                            <thead>
                            <tr role="row">
                                <th>Sl No</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Description</th>
                                <th>Photo</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr role="row" ng-repeat="testimonial in testimonials">
                                <td>{{$index+1}}</td>
                                <td>{{testimonial.name}}</td>
                                <td>{{testimonial.designation}}</td>
                                <td>{{testimonial.description}}</td>
                                <td class="center">
                                    <a class="example-image-link" href="{{testimonial.url}}" data-lightbox="images-{{testimonial.id}}" data-title="">
                                        <img src="{{testimonial.url}}" alt=""  style="width: 25px; max-height: 25px"/>
                                    </a>
                                </td>
                                <td class="center">
                                    <div  class="btn-group btn-group-xs" role="group">
                                        <button type="button" class="btn btn-info" ng-click="editTestimonial(testimonial)">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button  type="button" class="btn btn-danger" ng-click="deleteTestimonial(testimonial)">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
    <meta property="og:title" content="Dot Decor | Customised Furnitures & Interior designers - Buy Custom Design Home Interiors & Furnitures" />
    <meta property="og:site_name" content="Dot Decor | Customised Furnitures & Interior designers" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="Dot Decor - We are a vertically integrated interior design and furniture company. We custom design home interiors, show you how your new home will look in 3D, manufacture the furniture at our factory and undertake project execution." />
    <meta property="og:type" content="website" />
    <title>Dot Decor - Customised Furnitures & Interior designers - Buy Custom Design Home Interiors & Furnitures</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="Dot Decor - We are a vertically integrated interior design and furniture company. We custom design home interiors, show you how your new home will look in 3D, manufacture the furniture at our factory and undertake project execution.">
    <meta name="keywords" content="full house furnishing, full home interiors, home interiors, custom design furniture, interior designer, interior designs, modular kitchen, furniture online, wardrobes online, furniture online india, bedroom furniture, online furniture, home furniture online, living room furniture, office furniture" />
    <link rel="shortcut icon" href="<?= base_url();?>assets/images/favicon.png">
    <!-- ============ Style Sheets ============ -->
    <link rel="stylesheet" href="<?= base_url();?>assets/css/animate.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/et-line-icons.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/swiper.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/justified-gallery.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/revolution/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/revolution/css/navigation.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootsnav.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/style.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/responsive.css" />
</head>
<body>
    <header>
        <nav class="navbar navbar-default bootsnav bg-transparent nav-top-scroll">
            <div class="container nav-header-container height-100px xs-height-70px xs-padding-15px-lr">
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-8 text-left">
                        <a class="logo" href="<?= base_url();?>">
                            <img src="<?= base_url();?>assets/images/logo.png" data-at2x="<?= base_url();?>assets/images/logo.png" class="default" alt="Dot-Decor">
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 text-right">
                        <div class="hamburger-menu">
                            <div class="btn btn-hamburger border-none maine-btn">
                                <button class="navbar-toggle mobile-toggle" type="button" id="open-button" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>
                            <div class="hamburger-menu-wrepper xs-text-center">
                                <div class="hamburger-logo text-left"><a href="#" class="logo"><img src="<?= base_url();?>assets/images/logo.png" data-at2x="images/logo.png" alt="Dot-Decor"/></a></div>
                                <div class="btn btn-hamburger border-none">
                                    <button class="close-menu close-button-menu" id="close-button"></button>
                                </div>
                                <div class="animation-box">
                                    <div class="menu-middle">
                                        <div class="menu-wrapper display-table-cell vertical-align-middle text-left">
                                            <div class="equalize no-margin">
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 display-table">
                                                    <div class="display-table-cell vertical-align-middle">
                                                        <ul class="hamburger-menu-links alt-font">
                                                            <li><a href="<?= base_url();?>">Home</a></li>
                                                            <li><a href="<?= base_url();?>about">About Us</a></li>
                                                            <li><a href="<?= base_url();?>projects">Projects</a></li>
                                                            <li><a href="<?= base_url();?>contact">Contact</a></li>
                                                            <li><a href="<?= base_url();?>testimonials">Testimonials</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 display-table">
                                                    <div class="display-table-cell vertical-align-middle social-style-3">
                                                        <span class="text-extra-large text-deep-pink alt-font display-block margin-15px-bottom font-weight-400">Indian based Customised furniture and Interior Planners</span>
                                                        <span class="text-medium alt-font display-block font-weight-300 margin-15px-bottom line-height-30 fw">
                                                            Paravathani Building<br>
                                                            Ooty Road, Manjeri- 676 121,<br/>
                                                            Malappuram, Kerala - IND.<br/>
                                                            Mobile: +91-9995-424-211<br/>
                                                            Land line: 0483-2766-431<br/>
                                                            Email - <a href="mailto:art@dotdecor.in" class="text-white" target="_blank">art@dotdecor.in </a>
                                                        </span>
                                                        <div class="separator-line-horrizontal-medium-light2 bg-deep-pink margin-25px-tb xs-margin-15px-tb display-inline-block"></div>
                                                        <div class="social-icon-style-9">
                                                            <ul class="small-icon">
                                                                <li><a class="margin-20px-right facebook" href="https://www.facebook.com/dotatmos" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                                <li><a class="margin-20px-right google" href="https://www.instagram.com/dot.decor/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                                                <li><a class="margin-20px-right google" href="https://www.youtube.com/channel/UCFWR8SphsHI9maNByQNJ8SA" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <section class="wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 center-col margin-eight-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center last-paragraph-no-margin">
                    <h5 class="alt-font font-weight-600 text-extra-dark-gray text-uppercase">We love to talk</h5>
                </div>  
            </div>
            <div class="row">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12 text-center sm-margin-eight-bottom xs-margin-30px-bottom wow fadeInUp last-paragraph-no-margin">
                        <div class="display-inline-block margin-20px-bottom">
                            <div class="bg-extra-dark-gray icon-round-medium"><i class="icon-map-pin icon-medium text-white"></i></div>
                        </div>
                        <div class="text-extra-dark-gray text-uppercase text-small font-weight-500 alt-font margin-5px-bottom">Visit Our Office</div>
                        <p class="center-col">Paravathani Building, Ooty Road,<br>Manjeri-676 121, Malappuram</p>
                        <a href="#" class="text-uppercase text-deep-pink text-small margin-15px-top xs-margin-10px-top display-inline-block">GET DIRECTION</a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 text-center sm-margin-eight-bottom xs-margin-30px-bottom wow fadeInUp last-paragraph-no-margin" data-wow-delay="0.2s">
                        <div class="display-inline-block margin-20px-bottom">
                            <div class="bg-extra-dark-gray icon-round-medium"><i class="icon-chat icon-medium text-white"></i></div>
                        </div>
                        <div class="text-extra-dark-gray text-uppercase text-small font-weight-500 alt-font margin-5px-bottom">Let's Talk</div>
                        <p class="center-col">Mobile: +91-9995-42-42-11<br/>Phone: +91 9995-027-431</p>
                        <a href="#" class="text-uppercase text-deep-pink text-small margin-15px-top xs-margin-10px-top display-inline-block">call us</a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 text-center xs-margin-30px-bottom wow fadeInUp last-paragraph-no-margin" data-wow-delay="0.4s">
                        <div class="display-inline-block margin-20px-bottom">
                            <div class="bg-extra-dark-gray icon-round-medium"><i class="icon-envelope icon-medium text-white"></i></div>
                        </div>
                        <div class="text-extra-dark-gray text-uppercase text-small font-weight-500 alt-font margin-5px-bottom">E-mail Us</div>
                        <p class="center-col"><a href="mailto:art@dotdecor.in">art@dotdecor.in</a><br><a href="mailto:info@dotdecor.in">info@dotdecor.in</a></p>
                        <a href="#" class="text-uppercase text-deep-pink text-small margin-15px-top xs-margin-10px-top display-inline-block">send e-mail</a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 text-center wow fadeInUp last-paragraph-no-margin" data-wow-delay="0.6s">
                        <div class="display-inline-block margin-20px-bottom">
                            <div class="bg-extra-dark-gray icon-round-medium"><i class="icon-megaphone icon-medium text-white"></i></div>
                        </div>
                        <div class="text-extra-dark-gray text-uppercase text-small font-weight-500 alt-font margin-5px-bottom">Customer Services</div>
                        <p class="xs-width-100 center-col">Phone: +91  9526-26-62-62<br/>Land No: 0483-2766-431</p>
                        <a href="#" class="text-uppercase text-deep-pink text-small margin-15px-top xs-margin-10px-top display-inline-block">open ticket</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn" id="section-down">
        <div class="container">
            <div class="row equalize sm-equalize-auto">
                <div class="col-md-6 col-sm-12 col-xs-12 sm-margin-30px-bottom wow fadeInLeft">
                    <div class="padding-fifteen-all bg-light-gray border-radius-6 md-padding-seven-all xs-padding-30px-all height-100">
                        <span class="text-extra-dark-gray alt-font text-large font-weight-500 margin-25px-bottom display-block">Ready to get started?</span> 
                        <form id="contact-form" action="javascript:void(0)" method="post">
                            <div>
                                <div id="success-contact-form" class="no-margin-lr"></div>
                                <input type="text" name="name" id="name" placeholder="Name*" class="border-radius-4 bg-white medium-input">
                                <input type="text" name="email" id="email" placeholder="E-mail*" class="border-radius-4 bg-white medium-input">
                                <input type="text" name="subject" id="subject" placeholder="Subject" class="border-radius-4 bg-white medium-input">
                                <textarea name="comment" id="comment" placeholder="Your Message" rows="5" class="border-radius-4 bg-white medium-textarea"></textarea>
                                <button id="contact-us-button" type="submit" class="btn btn-small border-radius-4 btn-dark-gray">send message</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 last-paragraph-no-margin wow fadeInRight">
                    <div class="padding-ten-all bg-light-gray border-radius-6 md-padding-seven-all xs-padding-30px-all height-100 sm-text-center">
                        <span class="text-large font-weight-500 alt-font text-extra-dark-gray margin-5px-bottom display-block">Let's plan for a new project?</span>
                        <p>Let's create a great project together! Share your ideas with us. If you have any questions, Don't hesitate to write to us - we'll answer all of them.Or maybe you're here just to say "Hi"</p>
                        <iframe style="border:0;border: 1px solid #ffc000;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3914.894184029349!2d76.11756756451413!3d11.121258192088108!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba6366f7556b577%3A0xf6e93eb3f637d828!2sKurikkal&#39;s+Paravathani+Furniture!5e0!3m2!1sen!2sin!4v1551955845769" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <a href="#" class="btn btn-dark-gray btn-small text-extra-small border-radius-4 margin-25px-top">View in map</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center social-style-4 border round">
                    <span class="text-medium font-weight-500 text-uppercase display-block alt-font text-extra-dark-gray margin-30px-bottom">On social networks</span>
                    <div class="social-icon-style-4">
                        <ul class="margin-30px-top large-icon">
                            <li><a class="facebook" href="https://www.facebook.com/dotatmos" target="_blank"><i class="fa fa-facebook"></i><span></span></a></li>
                            <li><a class="twitter" href="https://www.instagram.com/dot.decor/" target="_blank"><i class="fa fa-instagram"></i><span></span></a></li> 
                            <li><a class="google" href="https://www.youtube.com/channel/UCFWR8SphsHI9maNByQNJ8SA" target="_blank"><i class="fa fa-youtube"></i><span></span></a></li>
                            <!--<li><a class="dribbble" href="http://dribbble.com/" target="_blank"><i class="fa fa-dribbble"></i><span></span></a></li>
                            <li><a class="linkedin" href="http://linkedin.com/" target="_blank"><i class="fa fa-linkedin "></i><span></span></a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <footer class="footer-modern padding-five-tb xs-padding-30px-tb">
        <div class="footer-widget-area padding-40px-bottom xs-padding-30px-bottom">
            <div class="container">
                <div class="row equalize xs-equalize-auto">
                    <div class="col-md-4 col-sm-12 col-xs-12 xs-text-center sm-margin-three-bottom xs-margin-20px-bottom display-table">
                        <div class="display-table-cell vertical-align-middle">
                            <h6 class="text-dark-gray width-70 md-width-100 no-margin-bottom">Indian based Customised furniture and Interior Planners</h6>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 xs-text-center xs-margin-20px-bottom display-table">
                        <div class="display-table-cell vertical-align-middle">
                            <span class="display-block">Paravathani Building<br>Ooty Road, Manjeri- 676 121,<br>Malappuram, Kerala - IND.</span>
                            <a href="mailto:art@dotdecor.in" title="art@dotdecor.in">art@dotdecor.in</a>   |   +91-9995-42-42-11
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 social-style-2 xs-text-center display-table">
                        <div class="display-table-cell vertical-align-middle">
                            <div class="social-icon-style-8">
                                <ul class="text-extra-small margin-20px-top xs-no-margin-bottom text-uppercase no-padding no-margin-bottom list-style-none">
                                    <li class="display-inline-block margin-10px-right">
                                        <a href="#"><img class="footer-logo" src="<?= base_url();?>assets/images/footer-logo.png" alt="Dot Decor"></a>
                                    </li>
                                    <!-- <li class="display-inline-block margin-10px-right">
                                        <a href="#" target="_blank"><img class="footer-logo2" src="<?= base_url();?>assets/images/paravathani.png" alt="Dot Decor"></a>
                                    </li> -->
                                </ul>
                            </div>
                            <div class="social-icon-style-8">
                                <ul class="text-extra-small margin-20px-top xs-no-margin-bottom text-uppercase no-padding no-margin-bottom list-style-none">
                                    <li class="display-inline-block margin-10px-right"><a href="https://www.facebook.com/dotatmos" target="_blank" title="Facebook">Facebook</a></li>
                                    <li class="display-inline-block margin-10px-right"><a href="https://www.instagram.com/dot.decor/" target="_blank" title="Instagram">Instagram</a></li>
                                    <li class="display-inline-block margin-10px-right"><a href="https://www.youtube.com/channel/UCFWR8SphsHI9maNByQNJ8SA" target="_blank" title="Twitter">YouTube</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="footer-bottom border-color-extra-light-gray border-top padding-40px-top xs-padding-30px-top">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 text-left text-small xs-text-center">Dot Decor - Customised Furnitures &copy; 2019</div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-right text-small xs-text-center">Proudly Designed by <a href="http://cloudbery.com/" target="_blank" title="Cloudbery Solutions"><img src="<?= base_url();?>assets/images/cloudbery.png" alt="Cloudbery Solutions"></a></div>
                </div>
            </div>
        </div>
    </footer>
    <p class="since">Since : 1989</p>
    <a class="scroll-top-arrow" href="javascript:void(0);"><i class="ti-arrow-up"></i></a>
    <!-- ======= CUSTOMER CONTACT FORM ======= -->
    <div class="buy-theme alt-font">
        <a href="#customer-contact" class="popup-with-form"><i class="fa fa-envelope"></i><span>Get an Estimate</span></a>
    </div>
    <form id="customer-contact" action="javascript:void(0)" method="post" class="white-popup-block mfp-hide col-md-5 no-padding center-col">
        <div class="padding-five-all bg-white border-radius-6 md-padding-seven-all">
            <div class="text-extra-dark-gray alt-font text-large font-weight-500 margin-30px-bottom">Get an Estimate</div> 
            <div>
                <div id="success-contact-form" class="no-margin-lr"></div>
                <input type="text" name="name" id="name" placeholder="Name*" class="input-bg">
                <input type="number" name="number" id="number" placeholder="Contact Number*" class="input-bg">
                <input type="number" name="abroad" id="abroad" placeholder="Contact Number (In Abroad)" class="input-bg">
                <input type="email" name="email" id="email" placeholder="E-mail" class="input-bg">
                <input type="text" name="location" id="location" placeholder="Project location*" class="input-bg">
                <textarea name="comment" id="comment" placeholder="Your Message" class="input-bg"></textarea>
                <button id="contact-us-button" type="submit" class="btn btn-small border-radius-4 btn-black">send message</button>
            </div>
        </div>
    </form>
    <!-- ======= DESIGNER CONTACT FORM ======= -->
    <div class="all-demo alt-font">
        <a href="#designer-contact" class="popup-with-form"><i class="fa fa-handshake-o"></i><span>Architects & Designers Enquiry</span></a>
    </div>
    <form id="designer-contact" action="javascript:void(0)" method="post" class="white-popup-block mfp-hide col-md-5 no-padding center-col">
        <div class="padding-five-all bg-white border-radius-6 md-padding-seven-all">
            <div class="text-extra-dark-gray alt-font text-large font-weight-500 margin-30px-bottom">Let’s Associate...</div> 
            <div>
                <div id="success-contact-form" class="no-margin-lr"></div>
                <input type="text" name="name" id="name" placeholder="Name*" class="input-bg">
                <input type="number" name="number" id="number" placeholder="Contact Number*" class="input-bg">
                <input type="text" name="firm" id="firm" placeholder="Your Company Name" class="input-bg">
                <input type="text" name="place" id="place" placeholder="Your Place" class="input-bg">
                <input type="date" name="date" id="date" placeholder="Choose your Meeting Date*" onClick="$(this).removeClass('placeholderclass')" class="input-bg dateclass placeholderclass">
                <textarea name="comment" id="comment" placeholder="Your Message" class="input-bg"></textarea>
                <button id="contact-us-button" type="submit" class="btn btn-small border-radius-4 btn-black">send message</button>
            </div>
        </div>
    </form>

    <!-- javascript libraries -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/modernizr.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/skrollr.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/smooth-scroll.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.appear.js"></script>
    <!-- menu navigation -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/bootsnav.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.nav.js"></script>
    <!-- animation -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/wow.min.js"></script>
    <!-- page scroll -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/page-scroll.js"></script>
    <!-- swiper carousel -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/swiper.min.js"></script>
    <!-- parallax -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.stellar.js"></script>
    <!-- magnific popup -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.magnific-popup.min.js"></script>
    <!-- portfolio with shorting tab -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/isotope.pkgd.min.js"></script>
    <!-- images loaded -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/imagesloaded.pkgd.min.js"></script>
    <!-- pull menu -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/classie.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/hamburger-menu.js"></script>
    <!-- counter  -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/counter.js"></script>
    <!-- fit video  -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.fitvids.js"></script>
    <!-- equalize -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/equalize.min.js"></script>
    <!-- justified gallery  -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/justified-gallery.min.js"></script>
    <!-- retina -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/retina.min.js"></script>
    <!-- revolution -->
    <script type="text/javascript" src="<?= base_url();?>assets/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/main.js"></script>
</body>
</html>
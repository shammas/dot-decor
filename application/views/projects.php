<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
    <meta property="og:title" content="Dot Decor | Customised Furnitures & Interior designers - Buy Custom Design Home Interiors & Furnitures" />
    <meta property="og:site_name" content="Dot Decor | Customised Furnitures & Interior designers" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="Dot Decor - We are a vertically integrated interior design and furniture company. We custom design home interiors, show you how your new home will look in 3D, manufacture the furniture at our factory and undertake project execution." />
    <meta property="og:type" content="website" />
    <title>Dot Decor - Customised Furnitures & Interior designers - Buy Custom Design Home Interiors & Furnitures</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="Dot Decor - We are a vertically integrated interior design and furniture company. We custom design home interiors, show you how your new home will look in 3D, manufacture the furniture at our factory and undertake project execution.">
    <meta name="keywords" content="full house furnishing, full home interiors, home interiors, custom design furniture, interior designer, interior designs, modular kitchen, furniture online, wardrobes online, furniture online india, bedroom furniture, online furniture, home furniture online, living room furniture, office furniture" />
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/favicon.png">
    <!-- ============ Style Sheets ============ -->
    <link rel="stylesheet" href="<?= base_url();?>assets/css/animate.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/swiper.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/justified-gallery.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/revolution/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/revolution/css/navigation.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/bootsnav.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/css/style.css" />
    <link rel="stylesheet" href="<?= base_url();?>assets/css/responsive.css" />
</head>
<body>
    <header>
        <nav class="navbar navbar-default bootsnav bg-transparent nav-top-scroll">
            <div class="container nav-header-container height-100px xs-height-70px xs-padding-15px-lr">
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-8 text-left">
                        <a class="logo" href="<?= base_url();?>">
                            <img src="<?= base_url();?>assets/images/logo.png" data-at2x="<?= base_url();?>assets/images/logo.png" class="default" alt="Dot-Decor">
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 text-right">
                        <div class="hamburger-menu">
                            <div class="btn btn-hamburger border-none maine-btn">
                                <button class="navbar-toggle mobile-toggle" type="button" id="open-button" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>
                            <div class="hamburger-menu-wrepper xs-text-center">
                                <div class="hamburger-logo text-left"><a href="#" class="logo"><img src="<?= base_url();?>assets/images/logo.png" data-at2x="images/logo.png" alt="Dot-Decor"/></a></div>
                                <div class="btn btn-hamburger border-none">
                                    <button class="close-menu close-button-menu" id="close-button"></button>
                                </div>
                                <div class="animation-box">
                                    <div class="menu-middle">
                                        <div class="menu-wrapper display-table-cell vertical-align-middle text-left">
                                            <div class="equalize no-margin">
                                                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 display-table">
                                                    <div class="display-table-cell vertical-align-middle">
                                                        <ul class="hamburger-menu-links alt-font">
                                                            <li><a href="<?= base_url();?>">Home</a></li>
                                                            <li><a href="<?= base_url();?>about">About Us</a></li>
                                                            <li><a href="<?= base_url();?>projects">Projects</a></li>
                                                            <li><a href="<?= base_url();?>contact">Contact</a></li>
                                                            <li><a href="<?= base_url();?>testimonials">Testimonials</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 display-table">
                                                    <div class="display-table-cell vertical-align-middle social-style-3">
                                                        <span class="text-extra-large text-deep-pink alt-font display-block margin-15px-bottom font-weight-400">Indian based Customised furniture and Interior Planners</span>
                                                        <span class="text-medium alt-font display-block font-weight-300 margin-15px-bottom line-height-30 fw">
                                                            Paravathani Building<br>
                                                            Ooty Road, Manjeri- 676 121,<br/>
                                                            Malappuram, Kerala - IND.<br/>
                                                            Mobile: +91-9995-424-211<br/>
                                                            Land line: 0483-2766-431<br/>
                                                            Email - <a href="mailto:art@dotdecor.in" class="text-white" target="_blank">art@dotdecor.in </a>
                                                        </span>
                                                        <div class="separator-line-horrizontal-medium-light2 bg-deep-pink margin-25px-tb xs-margin-15px-tb display-inline-block"></div>
                                                        <div class="social-icon-style-9">
                                                            <ul class="small-icon">
                                                                <li><a class="margin-20px-right facebook" href="https://www.facebook.com/dotatmos" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                                <li><a class="margin-20px-right google" href="https://www.instagram.com/dot.decor/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                                                <li><a class="margin-20px-right google" href="https://www.youtube.com/channel/UCFWR8SphsHI9maNByQNJ8SA" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <section class="wow fadeIn no-padding">
        <div class="container"> 
            <div class="row"> 
                <div class="col-md-12">
                    <div class="swiper-number-pagination swiper-container black-move">
                        <div class="bg-transparent-white opacity-full-dark z-index-5 sm-display-none"></div>
                        <div class="swiper-wrapper">
                            <!-- start slider item -->
                            <div class="swiper-slide">
                                <div class="slider-half-small-screen position-relative">
                                    <div class="slider-typography text-left">
                                        <div class="slider-text-middle-main">
                                            <div class="slider-text-middle">
                                                <span class="text-extra-large text-middle-line font-weight-300 text-deep-pink display-block margin-two-bottom width-50 xs-width-65">Creativity and Design</span>
                                                <h2 class="text-extra-dark-gray width-50 display-inline-block font-weight-300 letter-spacing-minus-1 md-width-70 xs-width-80">We're a design studio that believe in the ideas.</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- start slider item -->
                            <div class="swiper-slide">
                                <div class="slider-half-small-screen position-relative">
                                    <div class="slider-typography text-left">
                                        <div class="slider-text-middle-main">
                                            <div class="slider-text-middle">
                                                <span class="text-extra-large text-middle-line font-weight-300 text-deep-pink display-block margin-two-bottom width-50 xs-width-65">Identities and Experiences</span>
                                                <h2 class="text-extra-dark-gray width-50 display-inline-block font-weight-300 letter-spacing-minus-1 md-width-70 xs-width-80">We never replicate a piece of furniture twice.</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- start slider item -->
                            <div class="swiper-slide">
                                <div class="slider-half-small-screen position-relative">
                                    <div class="slider-typography text-left">
                                        <div class="slider-text-middle-main">
                                            <div class="slider-text-middle">
                                                <span class="text-extra-large text-middle-line font-weight-300 text-deep-pink display-block margin-two-bottom width-50 xs-width-65">Elevate and Empower</span>
                                                <h2 class="text-extra-dark-gray width-50 display-inline-block font-weight-300 letter-spacing-minus-1 md-width-70 xs-width-80">To make every home unique crafted furniture.</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-pagination swiper-number"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn no-padding-top sm-padding-50px-top xs-padding-30px-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12 no-padding xs-padding-15px-lr">
                    <div class="filter-content overflow-hidden">
                        <ul class="portfolio-grid work-3col hover-option8 gutter-medium">
                            <li class="grid-sizer"></li>
                            <?php
                            if (isset($projects) and $projects != false) {
                                foreach ($projects as $project) {
                                    ?>
                                    <!-- start portfolio item -->
                                    <li class="grid-item wow fadeInUp last-paragraph-no-margin">
                                        <a href="<?= base_url('project/' . $project->id . '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', character_limiter($project->name, 50,'')), '-');?>">
                                            <figure>
                                                <div class="portfolio-img">
                                                   <?php
                                                    if (isset($project->files) and $project->files != null) {
                                                        echo '<img src="' . current($project->files)->url . '" alt="">';
                                                    }else{
                                                        echo '<img  src="'.base_url().'assets/images/project-1.jpg" alt="">';
                                                    }?>
                                                </div>


                                                <figcaption>
                                                    <div class="portfolio-hover-main">
                                                        <div class="portfolio-hover-box vertical-align-middle">
                                                            <div
                                                                class="portfolio-icon alt-font font-weight-500 icon-medium text-deep-pink">
                                                                +
                                                            </div>
                                                            <span
                                                                class="text-medium alt-font text-extra-dark-gray margin-5px-bottom"><?= $project->name;?></span>

                                                            <p class="no-letter-spacing text-small text-medium-gray">
                                                                <?= $project->type;?></p>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </a>
                                    </li>
                                    <!-- end portfolio item -->
                                <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wow fadeIn padding-80px-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 center-col text-center">
                    <h6 class="alt-font text-extra-dark-gray text-uppercase font-weight-500 width-80 center-col margin-35px-bottom md-width-100 wow fadeInUp">We would love to hear from you .</h6>
                    <a href="<?= base_url();?>contact" class="btn btn-medium btn-rounded btn-deep-pink wow fadeInUp" data-wow-delay="0.2s">Let’s Meet</a>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer-modern padding-five-tb xs-padding-30px-tb">
        <div class="footer-widget-area padding-40px-bottom xs-padding-30px-bottom">
            <div class="container">
                <div class="row equalize xs-equalize-auto">
                    <div class="col-md-4 col-sm-12 col-xs-12 xs-text-center sm-margin-three-bottom xs-margin-20px-bottom display-table">
                        <div class="display-table-cell vertical-align-middle">
                            <h6 class="text-dark-gray width-70 md-width-100 no-margin-bottom">Indian based Customised furniture and Interior Planners</h6>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 xs-text-center xs-margin-20px-bottom display-table">
                        <div class="display-table-cell vertical-align-middle">
                            <span class="display-block">Paravathani Building<br>Ooty Road, Manjeri- 676 121,<br>Malappuram, Kerala - IND.</span>
                            <a href="mailto:art@dotdecor.in" title="art@dotdecor.in">art@dotdecor.in</a>   |   +91-9995-42-42-11
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 social-style-2 xs-text-center display-table">
                        <div class="display-table-cell vertical-align-middle">
                            <div class="social-icon-style-8">
                                <ul class="text-extra-small margin-20px-top xs-no-margin-bottom text-uppercase no-padding no-margin-bottom list-style-none">
                                    <li class="display-inline-block margin-10px-right">
                                        <a href="#"><img class="footer-logo" src="<?= base_url();?>assets/images/footer-logo.png" alt="Dot Decor"></a>
                                    </li>
                                    <!-- <li class="display-inline-block margin-10px-right">
                                        <a href="#" target="_blank"><img class="footer-logo2" src="<?= base_url();?>assets/images/paravathani.png" alt="Dot Decor"></a>
                                    </li> -->
                                </ul>
                            </div>
                            <div class="social-icon-style-8">
                                <ul class="text-extra-small margin-20px-top xs-no-margin-bottom text-uppercase no-padding no-margin-bottom list-style-none">
                                    <li class="display-inline-block margin-10px-right"><a href="https://www.facebook.com/dotatmos" target="_blank" title="Facebook">Facebook</a></li>
                                    <li class="display-inline-block margin-10px-right"><a href="https://www.instagram.com/dot.decor/" target="_blank" title="Instagram">Instagram</a></li>
                                    <li class="display-inline-block margin-10px-right"><a href="https://www.youtube.com/channel/UCFWR8SphsHI9maNByQNJ8SA" target="_blank" title="Twitter">YouTube</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="footer-bottom border-color-extra-light-gray border-top padding-40px-top xs-padding-30px-top">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 text-left text-small xs-text-center">Dot Decor - Customised Furnitures &copy; 2019</div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-right text-small xs-text-center">Proudly Designed by <a href="http://cloudbery.com/" target="_blank" title="Cloudbery Solutions"><img src="<?= base_url();?>assets/images/cloudbery.png" alt="Cloudbery Solutions"></a></div>
                </div>
            </div>
        </div>
    </footer>
    <p class="since">Since : 1989</p>
    <a class="scroll-top-arrow" href="javascript:void(0);"><i class="ti-arrow-up"></i></a>
    <!-- ======= CUSTOMER CONTACT FORM ======= -->
    <div class="buy-theme alt-font">
        <a href="#customer-contact" class="popup-with-form"><i class="fa fa-envelope"></i><span>Get an Estimate</span></a>
    </div>
    <form id="customer-contact" action="javascript:void(0)" method="post" class="white-popup-block mfp-hide col-md-5 no-padding center-col">
        <div class="padding-five-all bg-white border-radius-6 md-padding-seven-all">
            <div class="text-extra-dark-gray alt-font text-large font-weight-500 margin-30px-bottom">Get an Estimate</div> 
            <div>
                <div id="success-contact-form" class="no-margin-lr"></div>
                <input type="text" name="name" id="name" placeholder="Name*" class="input-bg">
                <input type="number" name="number" id="number" placeholder="Contact Number*" class="input-bg">
                <input type="number" name="abroad" id="abroad" placeholder="Contact Number (In Abroad)" class="input-bg">
                <input type="email" name="email" id="email" placeholder="E-mail" class="input-bg">
                <input type="text" name="location" id="location" placeholder="Project location*" class="input-bg">
                <textarea name="comment" id="comment" placeholder="Your Message" class="input-bg"></textarea>
                <button id="contact-us-button" type="submit" class="btn btn-small border-radius-4 btn-black">send message</button>
            </div>
        </div>
    </form>
    <!-- ======= DESIGNER CONTACT FORM ======= -->
    <div class="all-demo alt-font">
        <a href="#designer-contact" class="popup-with-form"><i class="fa fa-handshake-o"></i><span>Architects & Designers Enquiry</span></a>
    </div>
    <form id="designer-contact" action="javascript:void(0)" method="post" class="white-popup-block mfp-hide col-md-5 no-padding center-col">
        <div class="padding-five-all bg-white border-radius-6 md-padding-seven-all">
            <div class="text-extra-dark-gray alt-font text-large font-weight-500 margin-30px-bottom">Let’s Associate...</div> 
            <div>
                <div id="success-contact-form" class="no-margin-lr"></div>
                <input type="text" name="name" id="name" placeholder="Name*" class="input-bg">
                <input type="number" name="number" id="number" placeholder="Contact Number*" class="input-bg">
                <input type="text" name="firm" id="firm" placeholder="Your Company Name" class="input-bg">
                <input type="text" name="place" id="place" placeholder="Your Place" class="input-bg">
                <input type="date" name="date" id="date" placeholder="Choose your Meeting Date*" onClick="$(this).removeClass('placeholderclass')" class="input-bg dateclass placeholderclass">
                <textarea name="comment" id="comment" placeholder="Your Message" class="input-bg"></textarea>
                <button id="contact-us-button" type="submit" class="btn btn-small border-radius-4 btn-black">send message</button>
            </div>
        </div>
    </form>

    <!-- javascript libraries -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/modernizr.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/skrollr.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/smooth-scroll.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.appear.js"></script>
    <!-- menu navigation -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/bootsnav.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.nav.js"></script>
    <!-- animation -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/wow.min.js"></script>
    <!-- page scroll -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/page-scroll.js"></script>
    <!-- swiper carousel -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/swiper.min.js"></script>
    <!-- parallax -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.stellar.js"></script>
    <!-- magnific popup -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.magnific-popup.min.js"></script>
    <!-- portfolio with shorting tab -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/isotope.pkgd.min.js"></script>
    <!-- images loaded -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/imagesloaded.pkgd.min.js"></script>
    <!-- pull menu -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/classie.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/hamburger-menu.js"></script>
    <!-- counter  -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/counter.js"></script>
    <!-- fit video  -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.fitvids.js"></script>
    <!-- equalize -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/equalize.min.js"></script>
    <!-- justified gallery  -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/justified-gallery.min.js"></script>
    <!-- retina -->
    <script type="text/javascript" src="<?= base_url();?>assets/js/retina.min.js"></script>
    <!-- revolution -->
    <script type="text/javascript" src="<?= base_url();?>assets/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/js/main.js"></script>
</body>
</html>